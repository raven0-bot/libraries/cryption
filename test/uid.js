const { uuid } = require('../dist/index')

const assert = require('chai').assert;

/* eslint-disable no-undef */
describe('uuid', function() {
	it('should return a UUID', function() {

		assert.typeOf(uuid.generate(), 'string');
	}).timeout(5000);
});
