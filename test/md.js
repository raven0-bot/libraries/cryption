const { md5 } = require('../dist/index')
const { md4 } = require('../dist/index')

const assert = require('chai').assert;

/* eslint-disable no-undef */
describe('md', function() {
	it('should return a Hash in MD5 and MD4', function() {

		assert.equal(md5.Hash('owo', 'owo'), 'v2iwB8L9lXXPQotxJ8k4DQ==');
        assert.equal(md4.Hash('owo', 'owo'), 'yc3FKDSiylHwdVljV8GBrQ==');
	}).timeout(5000);
});
