const { base64 } = require('../dist/index');
const assert = require('chai').assert;

const string = 'owo';
const crypt = base64.encode(string);
const decrypt = base64.decode(crypt);

describe('base64', function() {
	it('should return a base64 encoded string', function() {

		assert.equal(crypt, 'b3dv')
        assert.equal(decrypt, string)
	}).timeout(5000);
});
