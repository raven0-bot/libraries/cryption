const { sha256 } = require('../dist/index')
const { sha512 } = require('../dist/index')

/* console.log(`[SHA256] ${sha256.Hash('owo', 'Hello!')}`)
console.log(`[SHA512] ${sha512.Hash('owo', 'Hello!')}`) */

const assert = require('chai').assert;

/* eslint-disable no-undef */
describe('sha', function() {
	it('should return a SHA256 & 512 Hash', function() {

		assert.equal(sha512.Hash('owo', 'owo'), 'mwggFNE2CqmsxBalTxCk6Cdzh1Cqgnly8gdT944EfLhHobky9CU55us28ojEsxdFBwlr5kP9oK64BSrCRCH+fA==');
		assert.equal(sha256.Hash('owo', 'owo'), 'QPJzepNlefvqYJ9jqPBFhWlqfcAtn4bMn0y5U7sUH28=');
	}).timeout(5000);
});
