const { hex } = require('../dist/index');
const assert = require('chai').assert;

const string = 'owo';
const crypt = hex.encode(string);
const decrypt = hex.decode(crypt);

/* eslint-disable no-undef */
describe('hex', function() {
	it('should return a hex encoded string', function() {

		assert.equal(crypt, '6f776f')
        assert.equal(decrypt, string)
	}).timeout(5000);
});
