declare const Buffer

/**
 * @param {string} str String to encode
 * @returns {string} Returns a string in hexadecimal
 */
export function encode (str: string): string {
  if (!str) throw new Error('No string provided.')

  const encoded: string = Buffer.from(str, 'utf-8').toString('hex')

  return encoded
}

/**
 * @param {string} str String to decode
 * @returns {string} Returns a string in utf-8
 */
export function decode (str: string): string {
  if (!str) throw new Error('No string provided.')

  const decoded: string = Buffer.from(str, 'hex').toString('utf-8')

  return decoded
}
