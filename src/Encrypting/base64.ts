declare const Buffer

/**
 * @param {string} str String to encode
 * @returns {string} Returns a string in base64
 */
export function encode (str: string): string {
  if (!str) throw new Error('No string provided.')

  const encoded: string = Buffer.from(str).toString('base64')

  return encoded
}

/**
 * @param {string} str String to encode
 * @returns {string} Returns a string in utf-8
 */
export function decode (str: string): string {
  if (!str) throw new Error('No string provided.')

  const decoded: string = Buffer.from(str, 'base64').toString('utf-8')

  return decoded
}
