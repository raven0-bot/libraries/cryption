import crypto from 'crypto'

/**
 * @param {string} secret Secret to hash the string
 * @param {string} str String to hash
 * @returns {string}
 */
export function Hash (secret: string, str: string) {
  if (!secret) throw new Error('No secret string provided')
  if (!str) throw new Error('Nothing to hash')

  const hasher = crypto.createHmac('md5', secret)
  const hashed = hasher.update(str).digest('base64')

  return hashed
}
