const base64 = require('./Encrypting/base64')
const hexadecimal = require('./Encrypting/hexadecimal')
const hex = require('./Encrypting/hexadecimal')
const md4 = require('./Hashing/md4')
const md5 = require('./Hashing/md5')
const sha256 = require('./Hashing/sha256')
const sha512 = require('./Hashing/sha512')
const uuid = require('./Misc/uuid')

module.exports = {
  base64,
  hexadecimal,
  hex,
  md4,
  md5,
  sha256,
  sha512,
  uuid
}
